package geiffel.da4.issuetracker.issue;

import geiffel.da4.issuetracker.exceptions.ResourceNotFoundException;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Primary
public class IssueJPAService implements IssueService{

    private IssueRepository issueRepository;

    public IssueJPAService(IssueRepository issueRepository) {
        this.issueRepository = issueRepository;
    }

    @Override
    public List<Issue> getAll() {
        return issueRepository.findAll();
    }

    @Override
    public Issue getByCode(Long l) {
        return null;
    }

    @Override
    public Issue create(Issue newIssue) {
        return null;
    }

    @Override
    public void update(Long code, Issue updatedIssue) throws ResourceNotFoundException {

    }

    @Override
    public void delete(Long code) throws ResourceNotFoundException {



    }


}
