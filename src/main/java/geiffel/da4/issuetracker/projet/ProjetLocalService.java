package geiffel.da4.issuetracker.projet;

import geiffel.da4.issuetracker.exceptions.ResourceAlreadyExistsException;
import geiffel.da4.issuetracker.exceptions.ResourceNotFoundException;
import geiffel.da4.issuetracker.utils.LocalService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Qualifier("local")
public class ProjetLocalService extends LocalService<Projet, Long> implements ProjetService {


    @Override
    protected String getIdentifier() {
        return "id";
    }


    public ProjetLocalService(List<Projet> projets) {
    }

    @Override
    public List<Projet> getAll() {
        return this.allValues;
    }

    @Override
    public Projet getById(Long l){
        IndexAndValue<Projet> found = this.findByProperty(l);
        return found.value();
    }

    @Override
    public Projet create(Projet newProjet) {
        try {
            this.getById(newProjet.getId());
            throw new ResourceAlreadyExistsException("Projet", newProjet.getId());
        } catch (ResourceNotFoundException e) {
            this.allValues.add(newProjet);
            return newProjet;
        }


    }

    @Override
    public void update(Long id, Projet updatedProjet){
        IndexAndValue<Projet> found = this.findByProperty(id);
        this.allValues.remove(found.index());
        this.allValues.add(found.index(), updatedProjet);

    }

    @Override
    public void delete(Long id){
        IndexAndValue<Projet> found = this.findByProperty(id);
        this.allValues.remove(found.index());
    }
}







