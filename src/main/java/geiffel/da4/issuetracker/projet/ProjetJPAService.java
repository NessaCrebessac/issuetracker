package geiffel.da4.issuetracker.projet;

import geiffel.da4.issuetracker.exceptions.ResourceNotFoundException;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Primary
public class ProjetJPAService implements ProjetService{


    private ProjetRepository projetRepository;

    public ProjetJPAService(ProjetRepository projetRepository){
        this.projetRepository = projetRepository;
    }

    @Override
    public List<Projet> getAll() {
        return projetRepository.findAll();
    }

    @Override
    public Projet getById(Long l){
        return null;
    }

    @Override
    public Projet create(Projet newProjet){
        return null;
    }

    @Override
    public void update(Long id, Projet updatedProjet) throws ResourceNotFoundException {
        Optional<Projet> projet = projetRepository.findById(id);
        if(projet.isEmpty()){
            throw new ResourceNotFoundException("Projet", id);
        }


    }

    @Override
    public void delete(Long id) throws ResourceNotFoundException{
        Optional<Projet> projet = projetRepository.findById(id);
        if(projet.isEmpty()){
            throw new ResourceNotFoundException("Projet", id);
        }
        projetRepository.delete(projet.get());

    }
}
