package geiffel.da4.issuetracker.projet;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/projets")
public class ProjetController {

    private ProjetService projetService;

    @Autowired
    public ProjetController(ProjetService projetService){this.projetService=projetService;}

    @GetMapping("/{id}")
    public Projet getProjetById(@PathVariable("id") Long value){

        return projetService.getById(value);
    }

    @PostMapping("")
    public ResponseEntity<Projet> createProjet(@RequestBody Projet projet){
        Projet createdProjet = projetService.create(projet);
        return ResponseEntity.created(URI.create("/projets/"+createdProjet.getId().toString())).build();
    }

    @PutMapping("/{id}")
    public ResponseEntity<Projet> updateProjet(@PathVariable Long id, @RequestBody Projet projet){
        projetService.update(id, projet);
        return  ResponseEntity.noContent().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Projet> deleteProjet(@PathVariable Long id){
        projetService.delete(id);
        return ResponseEntity.noContent().build();
    }
}
