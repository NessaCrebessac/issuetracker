package geiffel.da4.issuetracker.projet;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProjetService {
    Projet getById(Long value);

    Projet create(Projet projet);

    void update(Long id, Projet projet);

    void delete(Long id);

    List<Projet> getAll();
}
