package geiffel.da4.issuetracker.commentaire;

import geiffel.da4.issuetracker.exceptions.ResourceNotFoundException;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Primary
public class CommentaireJPAService implements CommentaireService{

    private CommentaireRepository commentaireRepository;

    public CommentaireJPAService(CommentaireRepository commentaireRepository) {
        this.commentaireRepository = commentaireRepository;
    }

    @Override
    public List<Commentaire> getAll() {

        return commentaireRepository.findAll();
    }

    @Override
    public Commentaire getById(Long id) {
        return null;
    }

    @Override
    public List<Commentaire> getAllByAuthorId(Long id) {
        return null;
    }

    @Override
    public List<Commentaire> getAllByIssueCode(Long code) {
        return null;
    }

    @Override
    public Commentaire create(Commentaire commentaire6) {
        return null;
    }

    @Override
    public void update(Long id, Commentaire toUpdate1) throws ResourceNotFoundException {
        Optional<Commentaire> commentaire = commentaireRepository.findById(id);
        if(commentaire.isEmpty()){
            throw new ResourceNotFoundException("Commentaire", id);
        }

    }

    @Override
    public void delete(Long id) throws ResourceNotFoundException {
        Optional<Commentaire> commentaire = commentaireRepository.findById(id);
        if (commentaire.isEmpty()){
            throw new ResourceNotFoundException("commentaire", id);
        }
        commentaireRepository.delete(commentaire.get());

    }
}
